import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/bpm/company/getList',
    method: 'post',
    data
  })
}

export function getCompanyDict(data) {
  return request({
    url: '/bpm/company/getCompanyIdAndNameDictList',
    method: 'post',
    data
  })
}

export function getCompanyByCode(data) {
  return request({
    url: '/bpm/company/getCompanyByCode/' + data,
    method: 'post',
    data
  })
}

export function getCompanyById(data) {
  return request({
    url: '/bpm/company/getCompanyById/' + data,
    method: 'post',
    data
  })
}
